import { Knex } from "knex";
import { STATUS } from "../src/utils/options";

exports.up = async (knex: Knex) => {
    return knex.schema.createTableIfNotExists("ticket", (table) => {
        table.uuid("id").primary();
        table.string("title").notNullable().unique();
        table.string("description").nullable();
        table.string("contact").nullable();
        table.string("information").nullable();
        table.enum("status", Object.values(STATUS)).defaultTo("pending");
        table.dateTime("created_at");
        table.dateTime("updated_at");
    });
};

exports.down = async (knex: Knex) => {
    return knex.schema.dropTable("ticket");
};