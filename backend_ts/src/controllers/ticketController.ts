import 'reflect-metadata';
import { Param, Body, Get, Post, Put, Delete,JsonController, QueryParams } from 'routing-controllers';
import Container, { Inject } from 'typedi'

import { TicketFilterForm, TicketForm } from '../utils/request';
import { TicketService } from '../service/ticketService';

@JsonController()
export class TicketController {

  @Inject()
  private readonly ticketService: TicketService
  constructor(){
    this.ticketService = Container.get(TicketService)
  }

  @Get('/tickets')
  tickets(@QueryParams() param: TicketFilterForm) {
    return this.ticketService.listTicket(param);
  }

  @Get('/ticket/:id')
  getTicketId(@Param('id') id: string) {
    return this.ticketService.getTicketId(id)
  }

  @Post('/ticket')
  createTicket(@Body() ticket: TicketForm) {
    return this.ticketService.createTicket(ticket);
  }

  @Put('/ticket/:id')
  updateTicket(@Param('id') id: string,@Body() ticket: TicketForm) {
    return this.ticketService.updateTicket(id,ticket)
  }

//   @Delete('/ticket/:id')
//   removeTicket(@Param('id') id: number) {
//     return 'Removing user...';
//   }
}