import { Service } from "typedi"
import { TicketEntity, TicketEntityFilter } from "../utils/entitys"
import { ITicketRepository } from "./interfaceRepository"
import { STATUS } from "../utils/options";
const { v4: uuidv4 } = require('uuid');

@Service()
export class MockTicketRepository implements ITicketRepository{
    table_name: string = "ticket"
    public static data: TicketEntity[] = [];
    public static empty(): void {
        MockTicketRepository.data = []
    }

    async listTicket(filter?: TicketEntityFilter): Promise<TicketEntity[]> {
        let filtered_data: TicketEntity[] = MockTicketRepository.data
        Object.entries(filter!).forEach(([key,val])=>{
            filtered_data = filtered_data.filter(tic => tic[key].includes(val))
        })
        return filtered_data
    }

    async getTicketById(id: string): Promise<TicketEntity>{
        const filtered_data = MockTicketRepository.data.filter((tic)=> tic.id === id)
        return filtered_data.length> 0 ? filtered_data[0] : undefined
    }

    async createTicket(ticket: TicketEntity): Promise<TicketEntity>{
        const now = new Date()
        ticket.id = uuidv4()
        ticket.created_at = now
        ticket.updated_at = now
        ticket.status = STATUS.PENDING
        MockTicketRepository.data.push(ticket)
        return this.getTicketById(ticket.id!)
    }

    async updateTicket(id: string, ticket: TicketEntity): Promise<TicketEntity>{
        ticket.updated_at = new Date()
        MockTicketRepository.data = MockTicketRepository.data.map((obj) => {
            if (obj.id === id){
                return {...obj,...ticket}
            }
            return obj
        })
        return this.getTicketById(id)
    }
}