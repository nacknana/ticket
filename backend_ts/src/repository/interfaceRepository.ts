import { TicketEntity, TicketEntityFilter } from "../utils/entitys"

export interface ITicketRepository {
    table_name: string
    listTicket(filter?: TicketEntityFilter): Promise<TicketEntity[]> 
    getTicketById(id: string): Promise<TicketEntity>
    createTicket(ticket: TicketEntity): Promise<TicketEntity>
    updateTicket(id: string, ticket: TicketEntity): Promise<TicketEntity>
}