import { Service } from "typedi"
import { TicketEntity, TicketEntityFilter } from "../utils/entitys"
import { knex } from "../connection/connection"
import { Knex } from "knex"
import { ITicketRepository } from "./interfaceRepository"
import { STATUS } from "../utils/options"
import { v4 as uuidv4 } from "uuid"

@Service()
export class TicketRepository implements ITicketRepository {
    table_name: string = "ticket"

    async listTicket(filter?: TicketEntityFilter): Promise<TicketEntity[]> {
        const query: Knex.QueryBuilder = knex(this.table_name)
        Object.entries(filter!).forEach(([key,val])=>{
            query.whereILike(key,`%${val}%`)
        })
        return await query
    }
    
    async getTicketById(id: string): Promise<TicketEntity>{
        const query: Knex.QueryBuilder = knex(this.table_name).where('id',id).first()
        return await query;
    }
    
    async createTicket(ticket: TicketEntity): Promise<TicketEntity>{
        const query: Knex.QueryBuilder = knex(this.table_name)
        const now = new Date()
        ticket.id = uuidv4()
        ticket.created_at = now
        ticket.updated_at = now
        ticket.status = STATUS.PENDING
        await query.insert(ticket)
        return await this.getTicketById(ticket.id!)
    }

    async updateTicket(id: string, ticket: TicketEntity): Promise<TicketEntity>{
        const query: Knex.QueryBuilder = knex(this.table_name)
        ticket.updated_at = new Date()
        await query.update(ticket).where('id',id)
        return await this.getTicketById(id)
    }
}