import { Inject, Service } from 'typedi'
import { TicketRepository } from '../repository/ticketRepository'
import { TicketResponse } from '../utils/response'
import { ErrorBadRequest, ErrorNotFound } from '../errors'
import { TicketModel } from '../utils/models'
import { TicketEntity, TicketEntityFilter } from '../utils/entitys'
import { STATUS } from '../utils/options'

@Service()
export class TicketService {
    @Inject()
    private ticketRepo: TicketRepository

    canChangeStatus(old_status: string,new_status: string): boolean{
        return (
                ![STATUS.ACCEPTED, STATUS.REJECTED].includes(old_status) &&
                !([STATUS.RESOLVED].includes(old_status) && new_status === 'rejected')
            )
    }

    async listTicket(filter: TicketEntityFilter): Promise<TicketResponse<TicketModel[]>>{
        const all_ticket = await this.ticketRepo.listTicket(filter)
        return new TicketResponse<TicketModel[]>({ data: all_ticket, message: '', status: true })
    }

    async getTicketId(id: string){
        const ticket = await this.ticketRepo.getTicketById(id)
        if (!ticket){
            throw new ErrorNotFound(`${id} not found`)
        }
        return new TicketResponse<TicketModel>({ data: ticket, message: '', status: true })
    }

    async createTicket(ticket: TicketModel){
        const all_ticket = await this.ticketRepo.listTicket({title:ticket.title!})
        if (all_ticket.length > 0){
            throw new ErrorBadRequest(`"${ticket.title}" is already exits.`)
        }
        const created_ticket = await this.ticketRepo.createTicket(ticket)
        return new TicketResponse<TicketModel>({data: created_ticket,message: "ticket created"})  
    }

    async updateTicket(id: string, ticket: TicketModel){
        const ticket_data = await this.ticketRepo.getTicketById(id)
        if (!ticket_data){
            throw new ErrorBadRequest(`${id} is not exits`)
        }
        const same_titles =  await this.ticketRepo.listTicket({title: ticket.title!})
        if ( same_titles.length >= 0 && !same_titles.every((tic)=> tic.id===id)){
            throw new ErrorBadRequest(`"${ticket.title}" is already exits.`)
        }
        if(!this.canChangeStatus(ticket_data.status!,ticket.status!)){
            throw new ErrorBadRequest(`${ticket_data.status} can't change to ${ticket.status}`)
        }
        const updated_ticket = await this.ticketRepo.updateTicket(id,ticket as TicketEntity)
        return new TicketResponse<TicketModel>({data: updated_ticket ,message: "ticket updated"})  
    }
}