export interface TicketEntity{
    id?: string;
    title?: string;
    description?: string;
    contact?: string;
    information?: string;
    status?: string;
    created_at?: Date;
    updated_at?: Date;
}

export interface TicketEntityFilter{
    id?: string;
    title?: string;
    description?: string;
    contact?: string;
    information?: string;
    status?: string;
    // created_at?: Date;
    // updated_at?: Date;
}