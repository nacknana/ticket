export const STATUS = {
    PENDING: "pending",
    ACCEPTED: "accepted",
    RESOLVED: "resolved",
    REJECTED: "rejected",
}
