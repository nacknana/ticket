import { ResponseBody } from "./models";

export class TicketResponse<T> implements ResponseBody{
    status?: boolean = true;
    data: T;
    total?: number;
    message?: string;
    error?: string;
    constructor(data: ResponseBody){
        this.data = data.data || []
        this.status = data.status || true
        if (data.data instanceof Array){
            this.total = data.data.length
        }
        if (data.error){
            this.error = data.error
            this.status = false
        }
        this.message = data.message || 'successfully'
    }
}