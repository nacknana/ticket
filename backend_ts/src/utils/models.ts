export interface ResponseBody<T=any>{
    status?: boolean;
    data: T;
    total?: number;
    message?: string;
    error?: any;
}

export interface TicketModel{
    id?: string;
    title?: string;
    description?: string;
    contact?: string;
    information?: string;
    status?: string;
    created_at?: Date;
    updated_at?: Date;
}