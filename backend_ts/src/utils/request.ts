import { IsEmpty, IsNotEmpty, IsOptional } from "class-validator";

export class TicketForm{

    @IsNotEmpty()
    public title!: string;

    @IsNotEmpty()
    public description!: string;

    @IsNotEmpty()
    public contact!: string;

    @IsNotEmpty()
    public information!: string;
    
    @IsOptional()
    public status!: string;
}

export class TicketFilterForm{

    @IsOptional()
    public id?: string;

    @IsOptional()
    public title?: string;

    @IsOptional()
    public description?: string;

    @IsOptional()
    public contact?: string;

    @IsOptional()
    public information?: string;

    @IsOptional()
    public status?: string;
}