import 'reflect-metadata';
// this shim is required
import { createExpressServer } from 'routing-controllers';
import { TicketController } from './controllers/ticketController';
import { HttpErrorHandler } from './errors/errors';


// creates express app, registers all controller routes and returns you express app instance
const app = createExpressServer({
  defaultErrorHandler: false,
  middlewares: [HttpErrorHandler],
  controllers: [TicketController], // we specify controllers we want to use
});


// run express application on port 3000
app.listen(3000);
console.log("listening on port 3000");
// console.log(process.env)