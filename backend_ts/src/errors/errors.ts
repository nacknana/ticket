
import { Middleware, ExpressErrorMiddlewareInterface, BadRequestError } from 'routing-controllers';

@Middleware({ type: 'after' })
export class HttpErrorHandler implements ExpressErrorMiddlewareInterface {
  error(error: any, request: any, response: any, next: (err: any) => any) {
    if (error instanceof BaseHttpError) {
      response.status(error.statusCode).json(error);
    }
    else if (error instanceof BadRequestError){
      response.status(error.httpCode).json(error);
    }
    next(error);
  }
}

export abstract class BaseHttpError extends Error {
    public statusCode: number;
    public name: string;
    public message: string;
    public errors?: {[key: string]: string | number};
  
    constructor(status: number, message: string, errorObject?: {[key: string]: string | number}) {
      super();
      this.name = this.constructor.name;
      this.statusCode = status;
      this.message = message;
      this.errors = errorObject;
    }
}

export class MyNotFoundError extends BaseHttpError{
  constructor(message: string = "data not found."){
      super(404,message)
  }
}

// export class MyValidateError extends BaseHttpError{
//   constructor(error: {[key: string]: string | number},message: string = "validation error"){
//     super(400,message,error)
//   }
// }

export class MyBadRequestError extends BaseHttpError{
  constructor(message: string = "bad request"){
    super(400,message)
}
}
