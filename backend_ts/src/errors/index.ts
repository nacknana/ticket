import { MyNotFoundError, MyBadRequestError } from "./errors";


export class ErrorNotFound extends MyNotFoundError{}
export class ErrorBadRequest extends MyBadRequestError{}