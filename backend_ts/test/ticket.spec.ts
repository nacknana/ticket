import 'reflect-metadata';
import * as chai from "chai";
import { TicketService } from "../src/service/ticketService";
import Container from "typedi";
import { TicketRepository } from "../src/repository/ticketRepository";
import { MockTicketRepository } from '../src/repository/mockTicketRepository';
import { TicketEntity } from '../src/utils/entitys';
import { TicketResponse } from '../src/utils/response';
import { TicketModel } from '../src/utils/models';
import { STATUS } from '../src/utils/options';

const testCreateTicket = async (serviceTicket: TicketService,ticket?: TicketEntity): Promise<TicketResponse<TicketModel>> => {
    const moc_ticket: TicketEntity = {
        title: "title",
        description: "description",
        contact: "contact",
        information:"information",
    }
    const res = await serviceTicket.createTicket(ticket ?? moc_ticket)
    return res
}
const testUpdateTicket = async (serviceTicket: TicketService,id: string,ticket?: TicketEntity): Promise<TicketResponse<TicketModel>> =>{
    const moc_ticket: TicketEntity = {
        title: "update_title",
        description: "update_description",
        contact: "update_contact",
        information:"update_information",
        status: STATUS.PENDING,
    }
    const res = await serviceTicket.updateTicket(id,ticket ?? moc_ticket)
    return res
}


describe("check all status not allow to change status", () => {
    before(()=>{
        chai.should()
        Container.set(TicketRepository,new MockTicketRepository())
    })

    afterEach(()=>{
        MockTicketRepository.empty()
    })

    it("test create success", async () => {
        const serviceTicket = Container.get(TicketService)
        const res = await testCreateTicket(serviceTicket)
        chai.assert.isTrue(res.status)
    });
    it('test update Ticket',async ()=>{
        const serviceTicket = Container.get(TicketService)
        const res_create = await testCreateTicket(serviceTicket)
        const res_update = await testUpdateTicket(serviceTicket,res_create.data.id)
        const con1 = res_update.data.title.startsWith('update_')
        chai.assert.equal(con1,true) // data will create on test create
        
    })
    it(`Test updateTicket to ${STATUS.ACCEPTED}`,async () => {
        const serviceTicket = Container.get(TicketService)
        const res_create = await testCreateTicket(serviceTicket)
        const res = await testUpdateTicket(serviceTicket,res_create.data.id,{status: STATUS.ACCEPTED})
        chai.assert.equal(res.data.status,STATUS.ACCEPTED)
    })
    it(`Test can\'t updateTicket status from ${STATUS.ACCEPTED} to ${STATUS.REJECTED}`,async () => {
        const serviceTicket = Container.get(TicketService)
        const res_create = await testCreateTicket(serviceTicket)
        const id = res_create.data.id
        await testUpdateTicket(serviceTicket,id,{status: STATUS.ACCEPTED})
        try{
            await testUpdateTicket(serviceTicket,id,{status: STATUS.REJECTED})
        }catch{}
        const data_ticket = await serviceTicket.getTicketId(id)
        chai.assert.equal(data_ticket.data.status,STATUS.ACCEPTED)
    })
    // it('Test get Ticket',async ()=>{
    //     const serviceTicket = Container.get(TicketService)
    //     const id = "dasdss"
    //     try{
    //         await testUpdateTicket(serviceTicket,id,{status: STATUS.REJECTED})
    //     }catch{}
    //     const ticket = await serviceTicket.getTicketId(id)
    //     chai.assert.isTrue(!ticket)
    // })

    
});