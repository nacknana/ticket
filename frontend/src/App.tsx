import { useState } from "react";
import { Ticket } from "./components/ticket/ticket";
import { Navbar } from "./components/navbar/navbar";
import "./App.css";

function App() {
  const [keySearch, setKeySearch] = useState<string | undefined>(undefined);

  const onSearchNavbar = (value: string | undefined) => {
    setKeySearch(value);
  };
  return (
    <>
      <Navbar onSearchSubmit={onSearchNavbar} />
      <Ticket searchBy={keySearch} />
    </>
  );
}

export default App;
