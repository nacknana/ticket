import { useState, useEffect, FC, ChangeEvent } from "react";
import { Box, Button, Dialog, DialogActions, DialogTitle, DialogContent, TextField } from "@mui/material";
import { TicketModel } from "../../../shared/model/ticket";
import { MySelect } from "../../mocule/select";
import { statusOptions } from "../../../shared/enum/option";
import serviceTicket from "../../../shared/service/ticket";

type TicketDialogProp = {
  isOpen: boolean;
  ticket?: TicketModel| null;
  onClose: () => void;
  onSubmit: () => void;
  onRequestClose?: () => void;
};

const stylesInput = {
  width: '100%',
  mt: '10px'
}

export const TicketDialog: FC<TicketDialogProp> = ({
  isOpen,
  ticket,
  onClose,
  onSubmit,
}) => {
  const [newTicket, setNewTicket] = useState<TicketModel | null>(null);
  const [newTicketError, setNewTicketError] = useState<TicketModel | null>(null);

  const validateTicket = () => {
    setNewTicketError(null)
    if (!newTicket?.title){
      setNewTicketError({title: 'required'})
    }
    else{
      if (ticket){
        updateTicket(ticket.id!, newTicket);
      }
      else{
        createTicket(newTicket);
      }
    }
    // setNewTicket(null)
    // onSubmit(newTicket!);
  };

  useEffect(() => {
    ticket? setNewTicket(ticket): setNewTicket(null)
    setNewTicketError(null)
  }, [ticket,isOpen]);

  const onCloseDialog = () => {
    onClose();
    // setNewTicket(null)
    ticket = null;
  }

  const createTicket = (tic: TicketModel) => {
    serviceTicket.createTicket(tic).then(res => {
      if (res.error && res.error.code === "23505" ){
        setNewTicketError({title: 'Title is already'})
      }
      else{
        onSubmit();
      }
    })
  }

  const updateTicket = (id: number,tic: TicketModel) => {
    serviceTicket.updateTicket({...tic,id:id}).then(res => {
      if (res.error && res.error.code === "23505" ){
        setNewTicketError({title: 'Title is already'})
      }
      else{
        onSubmit();
      }
    })
  }

  return (
    <div>
      <Dialog disableEscapeKeyDown  open={isOpen} onClose={onCloseDialog}>
          <DialogTitle style={{ fontSize: "40px", textAlign: "center" }}>
            {ticket ? "Update" : "Create"}
          </DialogTitle>
          <DialogContent>
            <Box sx={{
            width: 400,
            height: 300,
          }}>
              <TextField
              sx={stylesInput}
              id="outlined"
              label="title"
              type="text"
              variant="standard"
              error={newTicketError?.title? true:false}
              helperText={newTicketError?.title}
              value={newTicket?.title ?? ''}
              onChange={(e: ChangeEvent<HTMLInputElement>)=>{setNewTicket({...newTicket,title:e.target.value});setNewTicketError({title:undefined})}}
              />
              <TextField
              sx={stylesInput}
              id="outlined"
              label="contact"
              type="text"
              variant="standard"
              value={newTicket?.contact ?? ''}
              onChange={(e: ChangeEvent<HTMLInputElement>)=>{setNewTicket({...newTicket,contact:e.target.value})}}
              />
              <TextField
              sx={stylesInput}
              id="outlined"
              label="information"
              type="text"
              variant="standard"
              value={newTicket?.information ?? ''}
              onChange={(e: ChangeEvent<HTMLInputElement>)=>{setNewTicket({...newTicket,information:e.target.value})}}
              />
              <TextField
              rows={2}
              sx={stylesInput}
              id="outlined"
              label="description"
              type="text"
              variant="standard"
              value={newTicket?.description ?? ''}
              onChange={(e: ChangeEvent<HTMLInputElement>)=>{setNewTicket({...newTicket,description:e.target.value})}}
              />
              <MySelect 
              defaultValue={newTicket?.status}
              options={statusOptions}  
              onChange={(value)=> {setNewTicket({...newTicket,status:value})}}
              />
            </Box>
          </DialogContent>
          <DialogActions>
            <Button onClick={onCloseDialog}>Cancel</Button>
            <Button onClick={validateTicket}>{ticket ? "Save" : "Create"}</Button>
          </DialogActions>
      </Dialog>
    </div>
  );
};
