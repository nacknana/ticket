import { FC, useState,useEffect } from 'react';

import { FormControl,  InputLabel,  MenuItem, Select, SelectChangeEvent } from '@mui/material';
import { OptionKey } from '../../shared/model/share';



interface MySelectProps {
    onChange: (value:string) => void;
    options: OptionKey[];
    defaultValue?: string;
    txtTitle?: string;
}

export const MySelect: FC<MySelectProps> = ({ onChange,options,defaultValue,txtTitle }) => {
    const [data, setData] = useState(defaultValue??options[0].value);

    const handleChange = (event: SelectChangeEvent) => {
        setData(event.target.value);
        onChange(event.target.value)
      };

    useEffect(() => {
        defaultValue? setData(defaultValue): setData(options[0].value)
    }, [defaultValue, options]);

    return (
        <FormControl  variant="standard" sx={{ minWidth: 120,width: '100%',mt: '10px' }}>
            <InputLabel id="demo-simple-select-standard-label">{txtTitle??'status'}</InputLabel>
            <Select
            labelId="demo-simple-select-standard-label"
            id="demo-simple-select-standard"
            value={data}
            onChange={handleChange}
            displayEmpty
            inputProps={{ 'aria-label': 'Without label' }}
            >
            {options.map(
                ({value,text},i) =>{
                    return <MenuItem  key={i} value={value} >
                            {text}
                            </MenuItem>}
                )}
            </Select>
      </FormControl>
      );
};


