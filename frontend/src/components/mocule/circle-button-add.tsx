import React, { FC } from 'react';

import { IconButton, styled } from '@mui/material';
import AddIcon from '@mui/icons-material/Add';

const CirclePlusButton = styled(IconButton)(({ theme }) => ({
  borderRadius: '50%',
  width: '60px',
  height: '60px',
  backgroundColor: theme.palette.primary.main,
  color: theme.palette.primary.contrastText,
  '&:hover': {
    backgroundColor: theme.palette.primary.dark,
  },
  position: 'fixed',
  bottom: '20px',
  right: '20px',
  zIndex: 9999,
}));

interface CircleButtonProps {
    onClick: () => void;
}

export const CircleButton: FC<CircleButtonProps> = ({ onClick }) => {
    return (
        <CirclePlusButton onClick={onClick}>
          <AddIcon />
        </CirclePlusButton>
      );
};