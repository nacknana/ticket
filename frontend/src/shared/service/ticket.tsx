import axios from "axios"
import { TicketModel,TicketModelFilter } from "../model/ticket"
import { ResponseBody } from "../model/response"
import Config from "../../config"



const createTicket = (ticket: TicketModel): Promise<ResponseBody> =>{
    return axios.post<ResponseBody>(`${Config.HOST}${Config.URL.TICKET.CREATE}`,{...ticket})
    .then(({data})=> {
        return data
    }).catch((err)=> err);
}

const updateTicket = (ticket?: TicketModel): Promise<ResponseBody> =>{
    return axios.post<ResponseBody>(`${Config.HOST}${Config.URL.TICKET.UPDATE}`,{...ticket})
    .then(({data})=> {
        return data
    }).catch((err)=> err);
}

const searchTicket = (ticket?: TicketModelFilter): Promise<TicketModel[]>  =>{
    return axios.post<ResponseBody<TicketModel>>(`${Config.HOST}${Config.URL.TICKET.READ}`,{...ticket})
    .then(({data})=> {
        return data.data
    })
}

const serviceTicket ={
    searchTicket: searchTicket,
    createTicket: createTicket,
    updateTicket: updateTicket,
}

export default serviceTicket;