export interface TicketModel{
    id?: number;
    title?: string;
    description?: string;
    contact?: string;
    information?: string;
    status?: string;
    created_at?: string;
    updated_at?: string;
}

export interface TicketModelFilter{
    id?: number;
    title?: string;
    description?: string;
    contact?: string;
    information?: string;
    status?: string;
    order_by?: string;
}