export interface ResponseBody<T=any>{
    status: boolean;
    data: T[];
    message: string;
    error?: any;
}
