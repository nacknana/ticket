import React from 'react'
// import reactLogo from './assets/react.svg'
// import viteLogo from '/vite.svg'
import jaws_img from './assets/jaws.png'
import jaws_txt from './assets/jaws_txt.png'
import './App.css'

function App() {
  // const [count, setCount] = useState(0)

  // return (
  //   <>
  //     <div className='text-center'>
  //       <div className='title' style={{fontSize: '30px'}}>
  //         Title
  //       </div>
  //       <div className='sub-title' style={{fontSize: '24px'}}>
  //         Sub-Title
  //       </div>
  //       <div  style={{fontSize: '16px'}}>
  //         body
  //       </div>
  //       <div style={{backgroundColor: "#4AA2FF"}}>
  //         Primary
  //       </div>
  //       <div style={{backgroundColor: "#6BFF4A"}}>
  //         Secondary
  //       </div>
  //       <div style={{backgroundColor: "#67FFFA"}}>
  //         Info
  //       </div>
  //       <div style={{backgroundColor: "#F9F9F9"}}>
  //         Background
  //       </div>
  //       <div style={{backgroundColor: "#F5EBEA"}}>
  //         Forceground
  //       </div>
  //       <div style={{backgroundColor: "#FFA200"}}>
  //         Warnning
  //       </div>
  //       <div style={{backgroundColor: "#FF452C"}}>
  //         Error
  //       </div>
        
  //     </div>
  //   </>
  // )

  return (
    <>
      <div className='flex justify-center w-[100vw]'>
        <div id='poster-card' className='text-[20px] p-3' style={{backgroundColor: '#000c24'}}>
          <div id="top-box">
            <div className='text-center text-[27px]'>
              The terrifying motion picture <br />
              from the terrifying No.1 best seller.
            </div>
          </div>
          <div id="center-box">
            <div id='img-shark' className='w-[100%] flex justify-center'>
              <img src={jaws_img} className='w-[80%] mt-2' alt=""/>
            </div>
          </div>
          <div id='bottom-box' className='mt-2'>
            <div id="bottom-box-top" className='flex justify-center uppercase text-center'>
              <div className='my-auto mx-1'>
                <p>roy</p>
                <p>scheider</p>
              </div>
              <div className='my-auto mx-1'>
                <div>
                  <p>robert</p>
                  <p>shaw</p>
                </div>
                <div className='mt-4 flex justify-center'>
                  <img src={jaws_txt} className=' w-[5vw]' />
                </div>
              </div>
              <div className='my-auto mx-1'>
                <p>ricard</p>
                <p>dreyfuss</p>
              </div>
            </div>
            <div id="bottom-box-bottom" className='text-center text-[20px]'>
              <p>Co-staring LORRANE GARY • MURRAY HAMILION • A ZANICK/BROWN PRODUGTON</p>
              <p>Sceenplay by PETER BENCHLEY and CARL GOTLE • Based on the noned by PETER BENCHLEY • Misc by JOHN WILLIAMS</p>
              <p>Dreced by STEVEN SPIELBERG • Produced by RICHARD O. ZANUCK and DAVID BROWN • A UNIVERSAL PCTURE •</p>
            </div>
          </div>
        

        </div>

      </div>
    </>
  )
}

export default App



// export interface Wallet {
//   project_id: string;
//   wallet_amount: number;
//   type: string;
//   monthly_cycles_on: number;
//   created_at: string;
//   updated_at: string;
//   credit?: number;
//   payment_status: string;
//   payment_status_updated_at?: string;
//   overdue_cycles_allowed?: number;
//   overdue_duration?: number;
//   paused_duration?: number;
//   halted_duration?: number;
//   metadata?: Metadatum;
//   trial_period_end_at?: string;
//   payment_status_effective_duration: number;
//   payment_status_effective_until?: string;
// }

// interface Metadatum {
//   project_grade: string;
// }