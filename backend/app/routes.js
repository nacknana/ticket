const express = require('express')
const router = express.Router()

const ticket = require('./ticket')

router.post('/ticket/read', ticket.listTicket)
router.post('/ticket/create', ticket.createTicket)
router.post('/ticket/update', ticket.updateTicket)

module.exports = router