## Ticket App

**Client:** React TypeScript

**Server:** Node, Express

**Database** postgres

# Requirements
- Docker ( if don't have any database ) *can use other data base
- node.js 


## Run Locally


 To create database
 - open new terminal and run 

```bash
  cd database 
  docker-compose up -d
```

 To start server in dev mode
 - open new terminal and run
 ```bash
  cd backend 
  npm i 
  npm run dev
```

 To start frontend 
  - open new terminal and run
 ```bash
  cd frontend 
  npm i 
  npm start
```